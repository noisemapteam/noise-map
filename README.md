### Build

`./gradlew clean build`

### Run

`java -jar build/libs/server-1.0.0-SNAPSHOT.jar`


#### MQTT message
You can send message to free MQTT broker (http://www.hivemq.com/demos/websocket-client/).

Topic: "noiseMap/noiseData/krk" 

Example message:

    {
        "coords": {
            "latitude": "50.062011", 
            "longitude": "19.943860"
        }, 
        "timestamp": "2018-04-14T14:16:48+00:00", 
        "dB": "71.31"
    }
