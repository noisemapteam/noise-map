package noisemap.noisemapserver.config

import com.fasterxml.jackson.databind.ObjectMapper
import noisemap.noisemapserver.noisedata.NoiseDataFlux
import noisemap.noisemapserver.noisedata.NoiseDataWebSocket
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter


@Configuration
class WebConfig {

    @Bean
    fun handlerMapping(noiseDataFlux: NoiseDataFlux, mapper: ObjectMapper): HandlerMapping {
        val map = HashMap<String, WebSocketHandler>()
        map["/api/websocket/noiseData"] = NoiseDataWebSocket(noiseDataFlux.flux, mapper)

        val mapping = SimpleUrlHandlerMapping()
        mapping.urlMap = map
        mapping.order = -1
        return mapping
    }

    @Bean
    fun handlerAdapter(): WebSocketHandlerAdapter = WebSocketHandlerAdapter()
}
