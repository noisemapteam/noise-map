package noisemap.noisemapserver.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.*
import org.springframework.web.reactive.function.server.RequestPredicates.GET


@Configuration
class StaticHtmlConfig(@Value("classpath:/static/index.html") private val indexHtml: Resource) {

    @Bean
    fun routerFunction(): RouterFunction<*> {
        return RouterFunctions.resources("/**", ClassPathResource("static/"))
                // workaround solution for forwarding / to /index.html
                .andRoute(GET("/"), HandlerFunction { _: ServerRequest ->
                    ServerResponse.ok().contentType(TEXT_HTML).syncBody(indexHtml)
                })
    }
}
