package noisemap.noisemapserver.noisedata

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.ConnectableFlux
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxSink
import reactor.core.publisher.Mono


data class NoiseData(val coords: Coords, val timestamp: String, var dB: String?)

data class Coords(val latitude: String, val longitude: String)

@Component
class NoiseDataFlux(@Value("\${webSocket.noiseData.initCache}") cache: Int) {
    lateinit var fluxSink: FluxSink<NoiseData>
    val flux: Flux<NoiseData>

    init {
        val connectableFlux: ConnectableFlux<NoiseData> =
                Flux.create<NoiseData> { sink: FluxSink<NoiseData> -> fluxSink = sink }
                        .publish()
        connectableFlux.connect()
        flux = connectableFlux.cache(cache)
    }
}


@RestController("/api/noiseData")
class NoiseDataRestController(private val noiseDataFlux: NoiseDataFlux) {

    @PostMapping
    fun addNoiseData(@RequestBody noiseData: NoiseData) {
        noiseDataFlux.fluxSink.next(noiseData)
    }
}


class NoiseDataWebSocket(private val noiseDataFlux: Flux<NoiseData>,
                         private val mapper: ObjectMapper)
    : WebSocketHandler {

    override fun handle(session: WebSocketSession): Mono<Void> =
            session.send(
                    noiseDataFlux
                            .map { session.textMessage(mapper.writeValueAsString(it)) })
}
