package noisemap.noisemapserver.noisedata

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import org.springframework.web.client.RestOperations
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForEntity
import java.util.*
import java.util.concurrent.TimeUnit


val coords: List<Coords> = listOf(
        Coords(latitude = "50.061945", longitude = "19.937729"),
        Coords(latitude = "50.073750", longitude = "19.935863"),
        Coords(latitude = "50.079204", longitude = "19.921529"),
        Coords(latitude = "50.065734", longitude = "19.924061"),
        Coords(latitude = "50.048893", longitude = "19.932332"),
        Coords(latitude = "50.036063", longitude = "19.940395"),
        Coords(latitude = "50.053329", longitude = "19.933952"),
        Coords(latitude = "50.069166", longitude = "19.945223"),
        Coords(latitude = "50.057723", longitude = "19.958890"),
        Coords(latitude = "50.073550", longitude = "20.017997"),
        Coords(latitude = "50.067163", longitude = "19.989948"),
        Coords(latitude = "50.045566", longitude = "19.954927"),
        Coords(latitude = "50.032035", longitude = "19.924971"),
        Coords(latitude = "50.059176", longitude = "19.919451"),
        Coords(latitude = "50.085706", longitude = "19.955969"),
        Coords(latitude = "50.085706", longitude = "19.955969")
)

const val defaultTimestamp = "2018-04-14T14:16:48+00:00"

val mockDataProvider = {
    val rest: RestOperations = RestTemplate()
    val random = Random()
    var coordsIndex = 0
    while (true) {
        sendRequest(rest, coordsIndex, random)
        TimeUnit.SECONDS.sleep(1)
        coordsIndex = (coordsIndex + 1) % coords.size
    }
}

private fun sendRequest(rest: RestOperations, coordsIndex: Int, random: Random) {
    val dB: Double = 30.0 + (random.nextInt(500) / 10.0)
    val noiseData = NoiseData(coords = coords[coordsIndex], timestamp = defaultTimestamp, dB = dB.toString())
    rest.postForEntity<Void>(url = "http://localhost:8080/api/noiseData", request = noiseData)
}

@Component
class StartupApplicationListener(@Value("\${mockDataProvider.enable}") private val enableMockData: Boolean)
    : ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        log.info("Mock data provider enabled: $enableMockData")
        if (enableMockData) {
            Thread(mockDataProvider).start()
        }
    }

    companion object {
        val log: Logger = LoggerFactory.getLogger(StartupApplicationListener::class.java)
    }
}
