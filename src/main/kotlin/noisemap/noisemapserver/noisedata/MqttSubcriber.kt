package noisemap.noisemapserver.noisedata

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.integration.channel.DirectChannel
import org.springframework.integration.core.MessageProducer
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.MessageHandler
import java.util.*


@Configuration
class MqqtSubscriber(private val noiseDataFlux: NoiseDataFlux,
                     private val mapper: ObjectMapper,
                     @Value("\${mqtt.brokerUri}") private val brokerUri: String,
                     @Value("\${mqtt.topic}") private val topicName: String) {

    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    fun handler(): MessageHandler =
            MessageHandler { message ->
                val payload: String = message.payload as String
                val noiseData: NoiseData = mapper.readValue(payload)
                log.info("Noise Data from MQTT: $payload")
                noiseDataFlux.fluxSink.next(noiseData)
            }

    @Bean
    fun mqttInputChannel(): MessageChannel = DirectChannel()

    @Bean
    fun inbound(): MessageProducer {
        val clientId = "noise-map-service-" + UUID.randomUUID().toString().substring(0, 8)

        log.info("MQTT connection data: brokerUri=$brokerUri, clientId=$clientId, topicName=$topicName")
        val adapter = MqttPahoMessageDrivenChannelAdapter(brokerUri, clientId, topicName)
        adapter.setCompletionTimeout(5000)
        adapter.setConverter(DefaultPahoMessageConverter())
        adapter.setQos(1)
        adapter.outputChannel = mqttInputChannel()
        return adapter
    }

    companion object {
        val log: Logger = LoggerFactory.getLogger(StartupApplicationListener::class.java)
    }
}
