package noisemap.noisemapserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NoiseMapServerApplication

fun main(args: Array<String>) {
    runApplication<NoiseMapServerApplication>(*args)
}
